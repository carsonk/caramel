<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class acp_listener implements EventSubscriberInterface
{
	/** @var \phpbb\request\request */
	protected $request;
	/** @var \phpbb\template\template */
	protected $template;
	/** @var \phpbb\user */
	protected $user;

	/**
	* Provides core events subscribed to in phpBB.
	*
	* @return array   Contains core events with callback functions.
	*/
	static public function getSubscribedEvents()
	{
		return array(
			'core.acp_manage_forums_request_data'		=> 'forum_request',
			'core.acp_manage_forums_initialise_data'	=> 'forum_initialise',
			'core.acp_manage_forums_validate_data'      => 'forum_validate',
			'core.acp_manage_forums_display_form'		=> 'forum_display',
		);
	}

	/**
	* Constructor
	*
	* @param \phpbb\request\request   $request    Contains request tools.
	* @param \phpbb\template\template $template   The template object.
	* @param \phpbb\user              $user       The user object.
	*/
	public function __construct(\phpbb\request\request $request, \phpbb\template\template $template, \phpbb\user $user)
	{
		$this->request = $request;
		$this->template = $template;
		$this->user = $user;
	}

	/**
	* Add necessary settings to request info.
	*
	* @param object $event The event object.
	*/
	public function forum_request($event)
	{
		$forum_data = $event['forum_data'];

		$forum_data['forum_crml_enabled']           
			= $this->request->variable('crml_enabled', 1);

		$forum_data['forum_crml_topic_default']
			= $this->request->variable('crml_topic_default', 1);
		$forum_data['forum_crml_topic_incr']
			= (float) $this->request->variable('crml_topic_incr', 0.00);

		$forum_data['forum_crml_reply_default']
			= $this->request->variable('crml_reply_default', 1);
		$forum_data['forum_crml_reply_incr']
			= (float) $this->request->variable('crml_reply_incr', 0.00);

		$forum_data['forum_crml_per_word_default']
			= $this->request->variable('crml_per_word_default', 1);
		$forum_data['forum_crml_per_word_incr']
			= (float) $this->request->variable('crml_per_word_incr', 0.00);

		$forum_data['forum_crml_bonus_default']
			= $this->request->variable('crml_bonus_default', 1);
		$forum_data['forum_crml_bonus_incr']
			= (float) $this->request->variable('crml_bonus_incr', 0.00);
		$forum_data['forum_crml_bonus_prob']
			= (float) $this->request->variable('crml_bonus_prob', 0.00);

		$event['forum_data'] = $forum_data;
	}

	/**
	* Give default data from fields.
	*
	* @param object $event The event object.
	*/
	public function forum_initialise($event)
	{
		if($event['action'] == 'add')
		{
			$forum_data = $event['forum_data'];
			$forum_data = array_merge($forum_data, array(
				'forum_crml_enabled'	          => TRUE,
				'forum_crml_topic_default'        => TRUE,
				'forum_crml_topic_incr'           => 0.0,
				'forum_crml_reply_default'        => TRUE,
				'forum_crml_reply_incr'           => 0.0,
				'forum_crml_per_word_default'     => TRUE,
				'forum_crml_per_word_incr'        => 0.0,
				'forum_crml_bonus_default'        => TRUE,
				'forum_crml_bonus_incr'           => 0.0,
				'forum_crml_bonus_prob'           => 0.0,
			));
			$event['forum_data'] = $forum_data;
		}
	}

	/**
	* Validate forums stuff
	*/
	public function forum_validate($event)
	{
		$errors = $event['errors'];
		$forum_data = $event['forum_data'];

		if($forum_data['forum_crml_bonus_prob'] > 1.0 || $forum_data['forum_crml_bonus_prob'] < 0)
		{
			$errors[] = $this->user->lang['CRML_INVALID_PROB'];
		}

		$event['errors'] = $errors;
	}

	/**
	* Give necessary data to the template.
	*
	* @param object $event The event object.
	*/
	public function forum_display($event)
	{
		$tpl_data = $event['template_data'];
		$tpl_data['S_CARAMEL_ENABLED_FORUM'] = $event['forum_data']['forum_crml_enabled'];
		$tpl_data['S_CRML_TOPIC_DEFAULT'] = $event['forum_data']['forum_crml_topic_default'];
		$tpl_data['S_CRML_TOPIC_INCR'] = $event['forum_data']['forum_crml_topic_incr'];
		$tpl_data['S_CRML_REPLY_DEFAULT'] = $event['forum_data']['forum_crml_reply_default'];
		$tpl_data['S_CRML_REPLY_INCR'] = $event['forum_data']['forum_crml_reply_incr'];
		$tpl_data['S_CRML_PER_WORD_DEFAULT'] = $event['forum_data']['forum_crml_per_word_default'];
		$tpl_data['S_CRML_PER_WORD_INCR'] = $event['forum_data']['forum_crml_per_word_incr'];
		$tpl_data['S_CRML_BONUS_DEFAULT'] = $event['forum_data']['forum_crml_bonus_default'];
		$tpl_data['S_CRML_BONUS_INCR'] = $event['forum_data']['forum_crml_bonus_incr'];
		$tpl_data['S_CRML_BONUS_PROB'] = $event['forum_data']['forum_crml_bonus_prob'];
		$event['template_data'] = $tpl_data;
	}
}
