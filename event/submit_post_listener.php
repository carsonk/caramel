<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
* Listeners for post submission
*/
class submit_post_listener implements EventSubscriberInterface
{
	/** @var \phpbb\auth\auth */
	protected $auth;
	/** @var \phpbb\config\config */
	protected $config;
	/** @var \phpbb\db\driver\driver */
	protected $db;
	/** @var \phpbb\notification\manager */
	protected $notification_manager;
	/** @var \phpbb\user */
	protected $user;

	/** @var \carsonk\caramel\core\caramel_manager */
	protected $caramel_manager;

	/**
	* Constructor
	*
	* @param \phpbb\auth\auth $auth
	* @param \phpbb\config\config $config
	* @param \phpbb\db\driver\driver_interface $db
	* @param \phpbb\user $user
	* @param \carsonk\caramel\core\caramel_manager $caramel_manager
	*/
	public function __construct(
		\phpbb\auth\auth $auth, 
		\phpbb\config\config $config, 
		\phpbb\db\driver\driver_interface $db,
		\phpbb\notification\manager $notification_manager, 
		\phpbb\user $user, 
		\carsonk\caramel\core\caramel_manager $caramel_manager
	)
	{
		$this->auth = $auth;
		$this->config = $config;
		$this->db = $db;
		$this->notification_manager = $notification_manager;
		$this->user = $user;

		$this->caramel_manager = $caramel_manager;
	}

	/**
	* Gets core events subscribed to.
	*
	* @return array   Subscribed events.
	*/
	static public function getSubscribedEvents()
	{
		return array(
			'core.submit_post_end' => 'add_cash'
		);
	}

    /**
     * Adds cash to posts.
     * @param array $event The event context.
     */
	function add_cash($event) 
	{
		if($this->config['crml_enabled'] && $this->user->data['is_registered'])
		{
			$data = $event['data'];
			$mode = $event['mode'];

			$forum_id = $data['forum_id'];

			// Gets information about forum's cash stuff.
			$sql = 'SELECT forum_crml_enabled, forum_crml_topic_default, forum_crml_topic_incr, forum_crml_reply_default, forum_crml_reply_incr, forum_crml_edit_default, forum_crml_edit_incr, forum_crml_per_word_default, forum_crml_per_word_incr, forum_crml_bonus_default, forum_crml_bonus_incr, forum_crml_bonus_prob
				FROM ' . FORUMS_TABLE . "
				WHERE forum_id = $forum_id";
			$result = $this->db->sql_query($sql);
			$row = $this->db->sql_fetchrow($result);
			$this->db->sql_freeresult($result);

			if((int) $row['forum_crml_enabled'] === 1 && $this->auth->acl_get('u_crml_earn'))
			{
				// Figure out the increment based on the forum's settings.
				$increment = 0;
				switch($mode)
				{
					case 'post':
						$increment = ($row['forum_crml_topic_default']) 
							? $this->config['crml_topic_default'] : $row['forum_crml_topic_incr'];
						break;
					case 'reply':
					case 'quote':
						$increment = ($row['forum_crml_reply_default']) 
							? $this->config['crml_reply_default'] : $row['forum_crml_reply_incr'];
						break;
				}

				// TODO: Per-word increments.

				// Bonus increments.
				$bonus_probability = ($row['forum_crml_bonus_default']) 
					? $this->config['crml_bonus_probability'] : $row['forum_crml_bonus_prob'];
				$bonus_probability = floatval($bonus_probability);
				if($bonus_probability > 0.00)
				{
					$rand_number = mt_rand(0, 1000);
					$bonus_range = $bonus_probability * 1000;

					// If prob is .333, range is 333, and random num is 48, 
					// the user receives the bonus.
					$give_bonus = ($rand_number <= $bonus_range);
					if($give_bonus)
					{
						$bonus_amount = ($row['forum_crml_bonus_default']) 
							? $this->config['crml_bonus_default'] : $row['forum_crml_bonus_incr'];
						$this->caramel_manager->adjust_user_cash($this->user->data['user_id'], 'increment', $bonus_amount);

						$this->notification_manager->add_notifications(
							array('carsonk.caramel.notification.type.bonus'), 
							array(
								'post_id' => $data['post_id'],
								'user_id' => $this->user->data['user_id'],
								'amount' => $bonus_amount, 
							)
						);
					}
				}

				// Drop decimals if decimals are disabled.
				if($this->config['crml_enable_decimals'] == 0)
				{
					$increment = intval($increment);
				} 

				// Prevents SQL injection if table value gets fucked up.
				$increment = (float) $increment;
				$user_id = $this->user->data["user_id"];

				if(($this->caramel_manager->get_user_cash_value($user_id, TRUE) + $increment) > $this->caramel_manager->get_max_cash_value())
				{
					return;
				} 

				$sql = 'UPDATE ' . USERS_TABLE . '
					SET user_crml_cash = user_crml_cash + ' . $increment . '
					WHERE user_id = ' . $this->user->data['user_id'];
				$this->db->sql_query($sql);
			}
		}
	}
}