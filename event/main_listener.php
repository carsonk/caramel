<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/** 
* Global listener.
*/
class main_listener implements EventSubscriberInterface
{
	/** @var \phpbb\config\config */
	protected $config;
	/** @var \phpbb\db\driver\driver_interface */
	protected $db;
	/** @var \phpbb\request\request */
	protected $request;
	/** @var \phpbb\template\template */
	protected $template;
	/** @var \phpbb\user */
	protected $user;
	/** @var \carsonk\caramel\core\caramel_manager */
	protected $caramel_manager;

    /** @var string */
    protected $root_path;
    /** @var string */
    protected $php_ext;

    /**
     * Constructor
     *
     * @param \phpbb\config\config $config
     * @param \phpbb\db\driver\driver_interface $db
     * @param \phpbb\request\request $request
     * @param \phpbb\template\template $template
     * @param \phpbb\user $user
     * @param \carsonk\caramel\core\caramel_manager $caramel_manager
     * @param string $root_path
     * @param string $php_ext
     */
	public function __construct(
        \phpbb\config\config $config,
        \phpbb\db\driver\driver_interface $db,
        \phpbb\request\request $request,
        \phpbb\template\template $template,
        \phpbb\user $user,
        $caramel_manager,
        $root_path,
        $php_ext
    )
	{
		$this->config = $config;
		$this->db = $db;
		$this->request = $request;
		$this->template = $template;
		$this->user = $user;

		$this->caramel_manager = $caramel_manager;

        $this->root_path = $root_path;
        $this->php_ext = $php_ext;
	}

	/**
	* Gets core events subscribed to.
	*
	* @return array   Returns teh core events with their callbacks.
	*/
	static public function getSubscribedEvents()
	{
		return array(
			'core.common'     => 'global_calls',
			'core.user_setup' => 'user_setup',
		);
	}

	/**
	* Handles logic that needs to be called on every page.
	*
	* @param array $event   Array containing situational data.
	*/
	public function global_calls($event)
	{
		// Assign global template vars.
		$this->template->assign_vars(array(
			'S_CARAMEL'           => $this->config['crml_enabled'] ? TRUE : FALSE,
			'S_CARAMEL_ENABLED'   => $this->config['crml_enabled'] ? TRUE : FALSE, 
			'CRML_UNIT'           => $this->config['crml_unit_name']
		));
	}

	/**
	* Adds common lang data to every page.
	*
	* @param array $event   Array containing situational data.
	*/
	public function user_setup($event)
	{
		$crml_my_cash = 0;

		if($this->config['crml_enabled'] && $this->user->data['user_id'] != ANONYMOUS)
		{
			if($this->config['crml_login_increment'] != 0)
			{
				// Handle login increment.
				$this->caramel_manager->adjust_user_cash($this->user->data['user_id'], 'login');
			}

			$user_data = $event['user_data'];

			$sql_ary = array(
				'SELECT' => 'user_crml_cash',
				'FROM' => array(USERS_TABLE => 'u'),
				'WHERE' => 'user_id = ' . $this->user->data['user_id'],
			);
			$sql = $this->db->sql_build_query('SELECT', $sql_ary);
			$result = $this->db->sql_query($sql);
			$row = $this->db->sql_fetchrow($result);

			$crml_my_cash = ($this->config['crml_enable_decimals']) ?
                (float) $row['user_crml_cash'] : (int) $row['user_crml_cash'];

			$this->db->sql_freeresult($result);
		}

		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = array(
			'ext_name' => 'carsonk/caramel',
			'lang_set' => 'caramel_common',
		);
		$event['lang_set_ext'] = $lang_set_ext;

        $caramel_ucp_url = append_sid(
            $this->root_path . 'ucp.' . $this->php_ext,
            'i=-carsonk-caramel-ucp-caramel_module'
        );

		$this->template->assign_vars(array(
			'CRML_MY_CASH' => $crml_my_cash,
            'U_CARAMEL_UCP' => $caramel_ucp_url
        ));

		define('CRML_MY_CASH', $crml_my_cash);
	}
}
