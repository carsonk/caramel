<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\acp;

class caramel_info
{
	function module()
	{
		return array(
			'filename'   => '\carsonk\caramel\acp\caramel_module',
			'title'      => 'ACP_CARAMEL',
			'modes'      => array(
				'general'     => array(
					'title' => 'ACP_CARAMEL_GENERAL',
					'auth'  => 'ext_carsonk/caramel && acl_a_crml_manage_config',
					'cat'   => array('ACP_CARAMEL')
				)
			)
		);
	}
}