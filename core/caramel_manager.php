<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\core;

/**
* For keeping code DRY. :)
*/
class caramel_manager
{
	/** @var \phpbb\auth\auth */
	protected $auth;
	/** @var \phpbb\config\config */
	protected $config;
	/** @var \phpbb\db\driver\driver_interface */
	protected $db;
	/** @var \phpbb\log\log */
	protected $log;
	/** @var \phpbb\request\request */
	protected $request;
	/** @var \phpbb\template\template */
	protected $template;
	/** @var \phpbb\user */
	protected $user;

	/** @var string */
	protected $crml_log_table;
	/** @var string */
	protected $crml_log_types_table;
	/** @var string */
	protected $crml_tips_table;

	const MAX_CASH_VALUE = 99999999.99;

	/**
	* Constructor
	*
	* @param \phpbb\auth\auth $auth
	* @param \phpbb\config\config $config
	* @param \phpbb\db\driver\driver_interface $db
	* @param \phpbb\log\log $log
	* @param \phpbb\pagination $pagination
	* @param \phpbb\request\request $request
	* @param \phpbb\template\template $template
	* @param \phpbb\user $user
	* @param string $crml_log_table
	* @param string $crml_log_types_table
    * @param string $crml_tips_table
	*/
	public function __construct(
		\phpbb\auth\auth $auth, 
		\phpbb\config\config $config, 
		\phpbb\db\driver\driver_interface $db, 
		\phpbb\log\log $log, 
		\phpbb\pagination $pagination,
		\phpbb\request\request $request,
		\phpbb\template\template $template, 
		\phpbb\user $user, 
		$crml_log_table, 
		$crml_log_types_table,
		$crml_tips_table
	)
	{
		$this->auth = $auth;
		$this->config = $config;
		$this->db = $db;
		$this->log = $log;
		$this->request = $request;
		$this->template = $template;
		$this->user = $user;

		$this->crml_log_table = $crml_log_table;
		$this->crml_log_types_table = $crml_log_types_table;
		$this->crml_tips_table = $crml_tips_table;
	}

	/**
	* Logs a cash action.
	*
	* @param int       $type_name           Name of type from caramel_types_table.
	* @param string    $log_message         The message to include in the log.
	* @param float     $log_points          The points involved in the transaction.
	* @param int       $user_id_from        The user id of the transaction sender.
	* @param int       $user_id_to          The user id of receiving user
	* @return bool                          Success status.
	*/
	public function log_cash_action($type_name, $log_message, $log_points, $user_id_from, $user_id_to)
	{
		$log_type_id = $this->get_log_type((string) $type_name);

		if($log_type_id == 0) 
		{
			return FALSE;
		}

		$data = array(
			'user_id_from' => (int) $user_id_from,
			'user_id_to'   => (int) $user_id_to,
			'log_type_id'  => (int) $log_type_id,
			'log_message'  => (string) $log_message,
			'log_time'     => time(),
			'log_points'   => (int) $log_points
		);

		$sql = 'INSERT INTO ' . $this->crml_log_table . ' ' . 
			$this->db->sql_build_array('INSERT', $data);
		$this->db->sql_query($sql);

		return TRUE;
	}

	/**
	* Gets array of transaction logs.
	*
	* @param  int  $start      (optional) The item number to start at.
	* @param  int  $per_page   (optional) The number of items to grab.
	* @param  int  $user_id    (optional) Show only logs involving this user.
	* @return array            Array containing log rows.
	*/
	public function get_logs($start = 0, $per_page = 10, $user_id = NULL)
	{
		$ofTheJedi = array();

		$start = (int) $start;
		$per_page = (int) $per_page;

		// TODO: Make this more conforming to phpBB DB functions. Might be problems with ORDER BY when used on boards that aren't using MySQL. Not sure, though. Can't hurt to make it prettier though.
		$sql = 'SELECT * 
			FROM ' . $this->crml_log_table . ' ';
		if (is_int($user_id))
		{
			$sql .= 'WHERE user_id_from = ' . (int) $user_id . ' 
				OR user_id_to = ' . (int) $user_id . ' ';
		}
		$sql .= 'ORDER BY log_time DESC';
		$result = $this->db->sql_query_limit($sql, $per_page, $start);

		$rows = $this->db->sql_fetchrowset($result);
		$this->db->sql_freeresult($result);

		return $rows;
	}

	/** 
	* Gets a count of logs with user attached.
	*
	* @param int   $user_id   The user to get logs for.
	* @return int             The log count.
	*/
	public function get_log_count($user_id)
	{
		$sql_ary = array(
			'SELECT' => 'COUNT(log_id) AS log_count',
			'FROM'   => array($this->crml_log_table => 'l'),
			'WHERE'  => 'user_id_from = ' . (int) $user_id . ' OR user_id_to = ' . (int) $user_id,
		);
		$sql = $this->db->sql_build_query('SELECT', $sql_ary);
		$result = $this->db->sql_query($sql);
		$total_logs = (int) $this->db->sql_fetchfield('log_count');
		$this->db->sql_freeresult($result);
		return $total_logs;
	}

	/**
	* Gets a log type id by the name of the type.
	*
	* @param  string   $name     Name of the type to get id for.
	* @return int                The integer id of the type.
	*/
	public function get_log_type($name)
	{
		$data = array(
			'log_type_name' => $name
		);

		$sql = 'SELECT log_type_id
			FROM ' . $this->crml_log_types_table . '
			WHERE ' . $this->db->sql_build_array('SELECT', $data);
		$result = $this->db->sql_query($sql);
		$ofTheJedi = $this->db->sql_fetchrow();
		$this->db->sql_freeresult($result);

		if($ofTheJedi)
		{
			return $ofTheJedi['log_type_id'];
		}
		else
		{
			return 0;
		}
	}

	/**
	* Gets array of log types with their field names from the database
	*
	* @return array Array of field names as keys and values as keys.
	*/ 
	public function get_log_types()
	{
		$sql = 'SELECT * 
			FROM ' . $this->crml_log_types_table;
		$result = $this->db->sql_query($sql);
		$ofTheJedi = $this->db->sql_fetchrowset();
		$this->db->sql_freeresult($result);

		return $ofTheJedi; // Lol.
	}

	/**
	* Gets username of specific user ID.
	*
	* @param int $user_id   The user ID of the user.
	* @return string        The colored, HTMLified username string.
	*/
	public function get_username($user_id)
	{
		$sql = 'SELECT username, user_colour
			FROM ' . USERS_TABLE . '
			WHERE user_id = ' . (int) $user_id;
		$result = $this->db->sql_query($sql);

		$row = $this->db->sql_fetchrow($result);
		$this->db->sql_freeresult($result);

		return get_username_string('full', $user_id, $row['username'], $row['user_colour']);
	}

	/**
	 * Adjusts user's cash.
	 *
	 * @param int $user_id The id of the user to change.
	 * @param string $adjustment_type change|increment|donation|log
	 * @param int|float $amount (optional) The amount to change. Required for some types.
	 * @param var $type_data (optional) Data given specific to type.
	 * @return bool Success status
	 */
	public function adjust_user_cash($user_id, $adjustment_type, $amount = NULL, $type_data = NULL)
	{
		if(!is_numeric($user_id) || !$this->user->data['is_registered'])
		{
			return FALSE;
		}

		switch ($adjustment_type)
		{
			case 'change':
				// TODO: Finish adjust change case.
				$new_value = is_numeric($amount) ? (float) $amount : 0;
				break;
			case 'increment':
				$inc_value = is_numeric($amount) ? $amount : 0;
				$inc_value = ($this->config['crml_enable_decimals'])
					? (float) $inc_value : (int) $inc_value;

				// Don't udpate if exceeds max value.
				if(($this->get_user_cash_value($user_id) + $amount) > self::MAX_CASH_VALUE)
				{
					return FALSE;
				}

				$sql = 'UPDATE ' . USERS_TABLE . '
					SET user_crml_cash = user_crml_cash + ' . $inc_value . '
					WHERE user_id = ' . (int) $user_id;
				$this->db->sql_query($sql);

				break;
			case 'donation':
				if($this->config['crml_donations_enabled'])
				{
					$amount  = is_numeric($amount) ? (float) $amount : 0;
					$sending_uid = is_numeric($type_data) ? $type_data : $this->user->data['user_id'];

					if($amount <= 0)
					{
						return FALSE;
					}

					// Don't udpate if exceeds max value.
					if(($this->get_user_cash_value($user_id) + $amount) > self::MAX_CASH_VALUE)
					{
						return FALSE;
					}

					$sql = 'UPDATE ' . USERS_TABLE . '
						SET user_crml_cash = user_crml_cash + ' . $amount . '
						WHERE user_id = ' . $user_id;
					$this->db->sql_query($sql);

					$sql = 'UPDATE ' . USERS_TABLE . '
						SET user_crml_cash = user_crml_cash - ' . $amount . '
						WHERE user_id = ' . $sending_uid;
					$this->db->sql_query($sql);
				}

				break;
			case 'tip':
				if($this->config['crml_tips_enabled'])
				{
					$amount = $this->config['crml_tip_default'];
					$sending_uid = is_numeric($type_data) ? $type_data : $this->user->data['user_id'];

					if($amount <= 0)
					{
						return FALSE;
					}

					$sql = 'UPDATE ' . USERS_TABLE . '
						SET user_crml_cash = user_crml_cash + ' . $amount . '
						WHERE user_id = ' . $user_id;
					$this->db->sql_query($sql);

					$sql = 'UPDATE ' . USERS_TABLE . '
						SET user_crml_cash = user_crml_cash - ' . $amount . '
						WHERE user_id = ' . $sending_uid;
					$this->db->sql_query($sql);
				}

				break;
			case 'login':
				if($this->config['crml_login_increment'] != 0)
				{
					$login_increment = $this->config['crml_login_increment'];

					if(($this->request->variable($this->config['cookie_name'] . '_crml_login_flag', FALSE, TRUE, \phpbb\request\request_interface::COOKIE) == FALSE))
					{
						$sql = 'SELECT user_crml_last_login_incr
							FROM ' . USERS_TABLE . '
							WHERE user_id = ' . $this->user->data['user_id'];
						$result = $this->db->sql_query($sql);
						$row = $this->db->sql_fetchrow($result);
						$this->db->sql_freeresult($result);
						$last_login_incr = $row['user_crml_last_login_incr'];

						// Sets cookie expiring in twenty minutes.
						$this->user->set_cookie('crml_login_flag', 'test', time() + 1200, false);

						$dayAgo = time() - (24 * 60 * 60);
						$timeIsSet = isset($last_login_incr);

						if($timeIsSet && ($last_login_incr <= $dayAgo))
						{
							if(($this->get_user_cash_value($this->user->data['user_id']) + $login_increment) > self::MAX_CASH_VALUE)
							{
								return;
							}

							$sql = 'UPDATE ' . USERS_TABLE . '
								SET user_crml_cash = user_crml_cash + ' . $login_increment . ', user_crml_last_login_incr = ' . time() . '
								WHERE user_id = ' . $this->user->data['user_id'];
							$this->db->sql_query($sql);

							// TODO: Notify user that they received an increment for visiting.
						}
					}
				}
				
				break;
		}
	}

	/**
	* Gets a user's cash value by ID.
	* 
	* @param int  $user_id  The id of the user to get the cash value for.
	* @param bool $literal  Whether or not to get the literal decimal value for the user.
	* @return float   The user's cash value.
	*/
	public function get_user_cash_value($user_id, $literal = FALSE)
	{
		$user_id = (int) $user_id;

		$sql = 'SELECT user_crml_cash 
			FROM ' . USERS_TABLE . '
			WHERE user_id = ' . $user_id;
		$result = $this->db->sql_query($sql);
		$cash_value = $this->db->sql_fetchfield('user_crml_cash', $result);

		if($this->config['crml_enable_decimals'] || $literal)
		{
			$cash_value = (float) $cash_value;
		}
		else
		{
			$cash_value = (int) $cash_value;
		}

		return $cash_value;
	}

	/**
	* Get max cash value.
	*
	* @return float  The maximum cash value for a user.
	*/
	public function get_max_cash_value()
	{
		return self::MAX_CASH_VALUE;
	}
}