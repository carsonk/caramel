<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\mcp;

class manage_cash_info
{
	function module()
	{
		return array(
			'filename'   => '\carsonk\caramel\mcp\manage_cash_module',
			'title'      => 'MCP_CARAMEL',
			'modes'     => array(
				'manage_cash_search' => array(
					'title' => 'MCP_MANAGE_CASH_SEARCH',
					'auth'  => 'ext_carsonk/caramel && acl_m_crml_manage_user_cash',
					'cat'   => array('MCP_CARAMEL')
				),
				'manage_cash' => array(
					'title'   => 'MCP_MANAGE_CASH',
					'auth'    => 'ext_carsonk/caramel && acl_m_crml_manage_user_cash',
					'cat'     => array('MCP_CARAMEL'),
					'display' => false
				)
			) 
		);
	}

	function install()
	{
	}

	function uninstall()
	{
	}
}