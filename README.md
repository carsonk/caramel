# Caramel: Virtual Cash Extension #

Caramel is an extension for phpBB 3.1 that adds a "virtual cash" system to the board. Users can earn points for posting, donate cash to one another, and spend that cash using supported extensions.

### Installation ###

* On your instance of phpBB, copy the contents of Caramel into */ext/carsonk/caramel/*.
* In your ACP, go to the Customize tab and click "Enable" on "Caramel: Virtual Cash Extension".

### Features ###

* ACP-configurable cash system that allows users to earn and exchange points.
* Increments configurable for new topics and posts.
* Allows configuration of cash increments for individual forums.
* Allows users to donate cash to other users.
* MCP module for Managing User Cash.
* UCP Donation Logs.
* Configurable random bonus increments, with notifications.
* Tip button on posts, allowing users to quickly donate to one another.
