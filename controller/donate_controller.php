<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\controller;

/**
* Logic for donations
*/
class donate_controller
{
	/** @var \phpbb\auth\auth */
	protected $auth;
	/** @var \phpbb\config\config */
	protected $config;
	/** @var \phpbb\db\driver\driver_interface */
	protected $db;
	/** @var \phpbb\controller\helper */
	protected $helper;
	/** @var \phpbb\notification\manager */
	protected $notification_manager;
	/** @var \phpbb\request\request */
	protected $request;
	/** @var \phpbb\symfony_request */
	protected $symfony_request;
	/** @var \phpbb\template\template */
	protected $template;
	/** @var \phpbb\user\user */
	protected $user;

	/** @var \carsonk\caramel\core\caramel_manager */
	protected $caramel_manager;

	/** @var string */
	protected $log_table;
	/** @var string */
	protected $log_types_table;
	/** @var string */
	protected $tips_table;

	/** @var string */
	protected $root_path;
	/** @var string */
	protected $php_ext;

    /**
     * Constructor
     *
     * @param \phpbb\auth\auth $auth
     * @param \phpbb\config\config $config
     * @param \phpbb\db\driver\driver_interface $db
     * @param \phpbb\controller\helper $helper
     * @param \phpbb\notification\manager $notification_manager
     * @param \phpbb\request\request $request
     * @param \phpbb\symfony_request $symfony_request
     * @param \phpbb\template\template $template
     * @param \phpbb\user $user
     * @param \carsonk\caramel\core\caramel_manager $caramel_manager Caramel manager.
     * @param string $log_table
     * @param string $log_types_table
     * @param $tips_table
     * @param $root_path
     * @param $php_ext
     */
	public function __construct(
		\phpbb\auth\auth $auth, 
		\phpbb\config\config $config, 
		\phpbb\db\driver\driver_interface $db, 
		\phpbb\controller\helper $helper, 
		\phpbb\notification\manager $notification_manager, 
		\phpbb\request\request $request, 
		\phpbb\symfony_request $symfony_request, 
		\phpbb\template\template $template, 
		\phpbb\user $user, 
		\carsonk\caramel\core\caramel_manager $caramel_manager, 
		$log_table, 
		$log_types_table,
		$tips_table,
		$root_path,
		$php_ext
	)
	{
		$this->auth = $auth;
		$this->config = $config;
		$this->db = $db;
		$this->helper = $helper;
		$this->notification_manager = $notification_manager;
		$this->request = $request;
		$this->symfony_request = $symfony_request;
		$this->template = $template;
		$this->user = $user;

		$this->caramel_manager = $caramel_manager;

		$this->log_table = $log_table;
		$this->log_types_table = $log_types_table;
		$this->tips_table = $tips_table;

		$this->root_path = $root_path;
		$this->php_ext = $php_ext;

		$this->user->add_lang_ext('carsonk/caramel', 'caramel_donate');
	}

    /**
     * Handles donation page and submission.
     *
     * @param int $uid The receiving user's ID.
     * @return \Symfony\Component\HttpFoundation\Response Symfony's response.
     */
	public function donate($uid)
	{
		add_form_key('donate_form');

		$referer = $this->symfony_request->headers->get('referer');

		$error = '';

		if(!$this->config['crml_enabled'] || !$this->config['crml_donations_enabled'])
		{
			trigger_error($this->user->lang['CRML_DONATIONS_DISABLED']);
		}

		if(!$this->auth->acl_get('u_crml_donate'))
		{
			trigger_error($this->user->lang['CRML_DONATIONS_NO_PERM']);
		}

		$uid = intval($uid); // Just in case.

		$sql = 'SELECT user_id, user_type, username, user_colour, user_crml_cash
			FROM ' . USERS_TABLE . '
			WHERE user_id = ' . $uid;
		$result = $this->db->sql_query($sql);
		$row = $this->db->sql_fetchrow($result);
		$this->db->sql_freeresult($result);

		if(!$row || $row['user_type'] == USER_IGNORE)
		{
			trigger_error($this->user->lang('CRML_USER_NOT_EXIST'));
		}

		if($row['user_id'] == $this->user->data['user_id'])
		{
			trigger_error($this->user->lang('CRML_CANNOT_DONATE_SELF'));
		}

		$user_donate_to_username = get_username_string('full', $row['user_id'], $row['username'], $row['user_colour']);

		$user_row = $row;

		// User submitted case.
		if($this->request->is_set_post('submit'))
		{
			$amount_s = $this->request->variable('amount', 0);

			if(!is_numeric($amount_s))
			{
				$error = $this->user->lang('CRML_AMOUNT_MUST_NUMERIC');
			} 
			else 
			{
				$amount = ($this->config['crml_enable_decimals']) ? 
					floatval($amount_s) : floor(floatval($amount_s));

				if($amount <= 0 || $amount > $this->user->data['user_crml_cash'])
				{
					$error = $this->user->lang('CRML_AMOUNT_MUST_POSITIVE');
				}
				else if(($user_row['user_crml_cash'] + $amount) > $this->caramel_manager->get_max_cash_value())
				{
					$error = $this->user->lang('CRML_TOO_HIGH');
				}
				else if(!check_form_key('donate_form'))
				{
					// Form key was invalid.
					$error = $this->user->lang('FORM_INVALID');
				}
				else 
				{
					// Updates cash value.
					$sql = 'UPDATE ' . USERS_TABLE . '
						SET user_crml_cash = user_crml_cash + ' . $amount . '
						WHERE user_id = ' . $uid;
					$res = $this->db->sql_query($sql);

					$sql = 'UPDATE ' . USERS_TABLE .'
						SET user_crml_cash = user_crml_cash - ' . $amount . '
						WHERE user_id = ' . $this->user->data['user_id'];
					$res = $this->db->sql_query($sql);

					// Gives receiving user a notification.
					$this->notification_manager->add_notifications(
						array('carsonk.caramel.notification.type.donate'), 
						array(
							'user_id_from' => $this->user->data['user_id'],
							'user_id_to'   => $uid,
							'cash_amount'  => $amount,
							'message'      => $this->request->variable('message', '')
						)
					);

					$posted_referer = $this->request->variable('referer', '');

					// Logs transaction.
					$log_message = $this->user->lang('CRML_LOG_MESSAGE_DONATE',  $amount);
					$this->caramel_manager->log_cash_action('donate', $log_message, $amount, $this->user->data['user_id'], $uid);

					$return_message = '';
					if($posted_referer != '')
					{
						$return_message = '<a href="' . $posted_referer . '">' . $this->user->lang('CRML_DONATE_RETURN_LAST') . '</a>';
					}

					trigger_error($this->user->lang('CRML_SUCCESSFUL_DONATION') . '<br /><br />' . $return_message);
				}
			}
		}

		$this->template->assign_vars(array(
			'S_ERROR'       => (!empty($error)) ? TRUE : FALSE,
			'ERROR_MSG'     => $error,

			'DONATION_EXPLAINED' => $this->user->lang('CRML_DONATION_EXPLAIN', $user_donate_to_username),
			'DONATION_MESSAGE_IN' => $this->request->variable('message', ''),

			'U_REFERER' => $referer,
		));

		return $this->helper->render('donate.html', $this->user->lang('CRML_DONATE'));
	}

	/**
	* Tips another user for their post.
	*
	* @param int $post_id   The post to receive the tip.
	*/
	public function tip($post_id)
	{
		$json_response = new \phpbb\json_response();

		if($this->config['crml_enabled'] && $this->config['crml_tips_enabled'])
		{
            // Do they have permission to tip other users?
			if(!$this->auth->acl_get('u_crml_tip'))
			{
				$this->trigger_error_ajax('CRML_TIP_NO_PERMISSION');
			}

			$is_ajax = $this->request->is_ajax();

			if(!$is_ajax)
			{
				$this->trigger_error_ajax('CRML_TIP_NOT_AJAX');
			}

            // Ensure post is real.
			$sql_ary = array(
				'SELECT' => 'poster_id, post_text',
				'FROM' => array(POSTS_TABLE => 'p'),
				'WHERE' => 'post_id = ' . (int) $post_id,
			);
			$sql = $this->db->sql_build_query('SELECT', $sql_ary);
			$result = $this->db->sql_query($sql);
			$row = $this->db->sql_fetchrow($result);
			$this->db->sql_freeresult($result);

			if($row)
			{
				$poster_id = $row['poster_id'];

				$amount = $this->config['crml_tip_default'];

				if($poster_id == $this->user->data['user_id']) // Is the user attempting to tip themselves?
				{
					$this->trigger_error_ajax('CRML_TIP_NO_TIP_SELF');
				}

                if(CRML_MY_CASH < $amount) // Does the user have enough cash?
                {
                    $this->trigger_error_ajax('CRML_TIP_INSUFFICIENT');
                }

				$this->caramel_manager->adjust_user_cash($poster_id, 'tip');

				$post_text = $row['post_text'];
				$post_text_sample = strlen($post_text) > 15 
					? substr($post_text, 0, 15) . "..." : $post_text;

				$this->notification_manager->add_notifications(
					array('carsonk.caramel.notification.type.tip'), 
					array(
						'user_id_from'          => $this->user->data['user_id'],
						'user_id_to'            => $poster_id,
						'cash_amount'           => $this->config['crml_tip_default'],
						'post_id'               => $post_id,
						'post_message_sample'   => $post_text_sample
					)
				);

				$log_message = $this->user->lang('CRML_LOG_MESSAGE_TIP',  $amount);
				$this->caramel_manager->log_cash_action('tip', $log_message, $amount, $this->user->data['user_id'], $poster_id);

				$data = array(
					'tip_post_id' => $post_id,
					'user_id_from' => $this->user->data['user_id'],
					'user_id_to' => $poster_id,
					'tip_amount' => $amount,
				);
				$sql = 'INSERT INTO ' . $this->tips_table . ' ' . $this->db->sql_build_array('INSERT', $data);
				$this->db->sql_query($sql);

				if($is_ajax)
				{
					$data = array('SUCCESS' => TRUE);
				    $json_response->send($data);
				}
			}
			else
			{
				$this->trigger_error_ajax('CRML_TIP_POST_NOT_EXIST');
			}
		}
		else
		{
			$this->trigger_error_ajax('CRML_TIP_DISABLED');
		}
	}

	/**
	* Dynamically triggers an error based on whether or not the request is AJAX based.
	*
	* @param string $error_message_lang_key   The language key for the error message.
	*/
	private function trigger_error_ajax($error_message_lang_key)
	{
		$error_message = $this->user->lang($error_message_lang_key);

		if($this->request->is_ajax())
		{
			$data = array('ERROR_MSG' => $error_message);
		    $json_response = new \phpbb\json_response();
		    $json_response->send($data);
		}
		else
		{
			trigger_error($error_message_lang_key);
		}

		exit();
	}
}