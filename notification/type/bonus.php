<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\notification\type;

/**
* Notification handler for Caramel bonuses.
*/
class bonus extends \phpbb\notification\type\base
{
	protected $language_key = 'NOTIFICATION_BONUS';

	public static $notification_option = array(
		'lang'   => 'NOTIFICATION_BONUS_OPTION',
		'group'  => 'NOTIFICATION_GROUP_MISCELLANEOUS'
	);

	public function get_type()
	{
		return 'carsonk.caramel.notification.type.bonus';
	}

	public function is_available()
	{
		return true;
	}

	public static function get_item_id($data)
	{
		return (int) $data['post_id'];
	}

	public static function get_item_parent_id($data)
	{
		return 0;
	}

	public function find_users_for_notification($data, $options = array())
	{
		$options = array_merge(array(
			'ignore_users' => array(),
		), $options); // I don't think this is necessary?? Try removing...

		$users = array((int) $data['user_id']);

		return $this->check_user_notification_options($users, $options);
	}

	public function get_avatar()
	{
		return '';
	}

	public function get_title()
	{
		return $this->user->lang($this->language_key, $this->get_data('amount'), $this->config['crml_unit_name']);
	}

	public function users_to_query()
	{
		return array();
	}

	public function get_url()
	{
		return append_sid($this->phpbb_root_path . 'viewtopic.' . $this->php_ext, 'p=' . $this->get_data('post_id'));
	}

	public function get_redirect_url()
	{
		return $this->get_url();
	}

	public function get_email_template()
	{
		return false;
	}

	public function get_email_template_variables()
	{
		return array();
	}
	
	public function get_reference()
	{
		return '';
	}

	public function create_insert_array($data, $pre_create_data = array())
	{
		$this->set_data('post_id', $data['post_id']);
		$this->set_data('user_id', $data['user_id']);
		$this->set_data('amount', $data['amount']);

		return parent::create_insert_array($data, $pre_create_data);
	}
}