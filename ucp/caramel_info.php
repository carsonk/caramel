<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\ucp;

class caramel_info
{
	function module()
	{
		return array(
			'filename'   => '\carsonk\caramel\ucp\carmel_module',
			'title'      => 'UCP_CARAMEL',
			'modes'     => array(
				'cash_logs' => array(
					'title' => 'UCP_CRML_CASH_LOGS',
					'auth'  => 'ext_carsonk/caramel',
					'cat'   => array('UCP_CARAMEL')
				),
			) 
		);
	}
}