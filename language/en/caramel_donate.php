<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'CRML_DONATIONS_DISABLED'   => 'Donations are disabled',
	'CRML_DONATIONS_NO_PERM'    => 'You do not have permissions to make donations.',
	'CRML_USER_NOT_EXIST'       => 'The user you are trying to donate to does not exist, or this user cannot accept donations.',
	'CRML_CANNOT_DONATE_SELF'   => 'You cannot donate to yourself.',
	'CRML_AMOUNT_MUST_NUMERIC'  => 'Amount must be numeric.',
	'CRML_AMOUNT_MUST_POSITIVE' => 'Amount must be a positive number that is less than the amount of cash you have.',

	'CRML_DONATION_AMOUNT'      => 'Amount',
	'CRML_DONATION_MESSAGE'     => 'Message',

	'CRML_DONATION_EXPLAIN'     => 'You currently are currently donating to %s.',

	'CRML_SUCCESSFUL_DONATION'  => 'Donation was succesful.',

	'CRML_LOG_MESSAGE_DONATE'   => 'A donation of %d cash was made.',
	'CRML_LOG_MESSAGE_TIP'      => 'A tip of %d cash was made.',

	'CRML_DONATE_RETURN_INDEX' => 'Return to index',
	'CRML_DONATE_RETURN_LAST' => 'Return to previous page',

	'CRML_TIP_DISABLED' => 'Tipping is disabled.',
	'CRML_TIP_POST_NOT_EXIST' => 'The passed post does not exist.',
	'CRML_TIP_NO_PERMISSION' => 'You do not have permission to tip other users.',
	'CRML_TIP_NOT_AJAX' => 'Tipping must be done via AJAX.',
	'CRML_TIP_NO_TIP_SELF' => 'You cannot tip yourself.',
    'CRML_TIP_INSUFFICIENT' => 'You do not have enough to tip.',
));
