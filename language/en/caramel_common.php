<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'CRML_CARAMEL'   => 'Caramel',
	'CRML_MANAGE'    => 'Manage',
	'CRML_DONATE'    => 'Donate',
	'CRML_TIP'       => 'Tip',
	'CRML_TIP_POST'  => 'Quickly tip user with %d %s for this post.',

	'NOTIFICATION_DONATE'        => 'You have received a donation of %d %s from %s!',
	'NOTIFICATION_DONATE_OPTION' => 'Someone donates you cash', 
	'NOTIFICATION_BONUS'         => 'You received a bonus of %d %s!',
	'NOTIFICATION_BONUS_OPTION'  => 'You receive a cash bonus on a post',
	'NOTIFICATION_TIP'           => '<strong>You received a tip of %d %s from %s for one of your posts!</strong>',
	'NOTIFICATION_TIP_OPTION'    => 'You receive a tip of for a post',

	'LOG_CHANGE_CASH' => 'Changed cash value for “%1$s” to %2$d',

	'L_CRML_TIP_AJAX_ISSUE' => 'There was an issue with the Tip request.',
	'CRML_TOO_HIGH' => 'User cash values cannot exceed 99,999,999.99.',
));
