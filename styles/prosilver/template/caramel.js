/**phpbb.addAjaxCallback('carsonk_caramel_tip', function(data) {
	if(data.SUCCESS) {
    	alert('We good.');
	} else if(data.ERROR_MSG) {
		alert(data.ERROR_MSG);
	} else {
		alert("{L_CRML_TIP_AJAX_ISSUE}");
	}
});*/

(function ($) { 
	$(document).ready(function() {
		$('.caramel-tip').click(function(e) {
			e.preventDefault();

			var tip_url = $(this).attr("href");

			if(tip_url)
			{
				$(this).fadeOut();

				$.post(tip_url, {}, function(data)
				{
					console.log(data);
					if(data.SUCCESS) {
						// Success case.
					} else if(data.ERROR_MSG) {
						alert(data.ERROR_MSG);
					} else {
						alert("{L_CRML_TIP_AJAX_ISSUE}");
					}
				}, "json");
			}

			return false;
		});
	});
})(jQuery);