<?php
/**
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\migrations\converters;

class ultimate_points extends \phpbb\db\migration\migration
{
	static public function depends_on()
	{
		return array(
			'\carsonk\caramel\migrations\v1\m2_data_foundations'
		);
	}

	public function effectively_installed()
	{
		$points_log_exists = $this->db_tools->sql_table_exists($this->table_prefix . 'points_log');
		$user_points_exists = $this->db_tools->sql_column_exists($this->table_prefix . 'users', 'user_points');
		return !$points_log_exists || !$user_points_exists;
	}

	public function update_data()
	{
		return array(
			array('custom', array(array($this, 'import_data')))
		);
	}

	public function import_data()
	{
		$sql = 'UPDATE users SET user_crml_cash = user_points';
		$this->db->sql_query($sql);
	}
}