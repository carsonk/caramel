<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\migrations\v1;

class m3_add_mod_log_type extends \phpbb\db\migration\migration
{
	static public function depends_on()
	{
		return array(
			'\carsonk\caramel\migrations\v1\m2_data_foundations'
		);
	}

	public function update_data()
	{
		return array(
			// Data into custom tables
			array('custom', array(array($this, 'add_log_type_data')))
		);
	}

	public function revert_data()
	{
		$this->remove_log_type_data();
		return array();
	}

	public function add_log_type_data()
	{
		$sql_array = array(
			array(
				'log_type_name' => 'mod',
				'log_type_ucp_visible' => 1
			),
		);

		$this->db->sql_multi_insert($this->table_prefix . 'caramel_log_types', $sql_array);
	}

	public function remove_log_type_data()
	{
		$sql = 'DELETE * FROM ' . $this->table_prefix . 'caramel_log_types WHERE log_type_name = mod';
		$this->sql_query($sql);
	}
}
