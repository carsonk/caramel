<?php
/**
*
* Caramel
*
* @copyright (c) 2017 carsonk
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace carsonk\caramel\migrations\v1;

class m2_data_foundations extends \phpbb\db\migration\migration
{
	static public function depends_on()
	{
		return array(
			'\carsonk\caramel\migrations\v1\m1_schema_foundations'
		);
	}

	public function update_data()
	{
		$data_sets = array(
			// TODO: Disable enabling by default for production release.

			// Feature settings
			array('config.add', array('crml_enabled', 1)),
			array('config.add', array('crml_unit_name', 'Cash')),
			array('config.add', array('crml_enable_decimals', 1)),
			array('config.add', array('crml_donations_enabled', 1)),
			array('config.add', array('crml_tips_enabled', 1)),
			array('config.add', array('crml_bonus_probability', .010)),

			// Cash defaults
			array('config.add', array('crml_topic_default', 10)),
			array('config.add', array('crml_reply_default', 5)),
			array('config.add', array('crml_edit_default', 0)), 
			array('config.add', array('crml_per_word_default', 0)),
			array('config.add', array('crml_login_increment', 5)),
			array('config.add', array('crml_tip_default', 3)),
			array('config.add', array('crml_bonus_default', 3)),

			array('permission.add', array('a_crml_manage_config')),

			array('permission.add', array('m_crml_view_logs')),
			array('permission.add', array('m_crml_manage_user_cash')),

			// Permission creation
			array('permission.add', array('u_crml_earn')),
			array('permission.add', array('u_crml_donate')),
			array('permission.add', array('u_crml_tip')),
			array('permission.add', array('u_crml_bonus')),
			array('permission.add', array('u_crml_login_increment')),

			// ACP Module additions
			array('module.add', array('acp', 'ACP_CAT_DOT_MODS', 'ACP_CARAMEL')),
			array('module.add', array(
				'acp', 'ACP_CARAMEL', array(
					'module_basename' => '\carsonk\caramel\acp\caramel_module',
					'modes' => array('general')
				)
			)),

			// MCP Module Additions
			array('module.add', array('mcp', '', 'MCP_CARAMEL')),
			array('module.add', array(
				'mcp', 
				'MCP_CARAMEL', 
				array(
					'module_basename'    => '\carsonk\caramel\mcp\manage_cash_module',
					'modes' => array('manage_cash_search', 'manage_cash')
				)
			)),

			array('module.add', array('ucp', '', 'UCP_CARAMEL')),
			array('module.add', array(
				'ucp',
				'UCP_CARAMEL',
				array(
					'module_basename' => '\carsonk\caramel\ucp\caramel_module',
					'modes' => array('cash_logs'),
				)
			)),

			// Data into custom tables
			array('custom', array(array($this, 'add_log_type_data')))
		);

		if($this->role_exists('ROLE_ADMIN_FULL'))
		{
			$data_sets[] = 
			array('permission.permission_set', array('ROLE_ADMIN_FULL', 'a_crml_manage_config'));
		}

		if($this->role_exists('ROLE_ADMIN_STANDARD'))
		{
			$data_sets[] = 
			array('permission.permission_set', array('ROLE_ADMIN_STANDARD', 'a_crml_manage_config'));
		}

		if($this->role_exists('ROLE_MOD_FULL'))
		{
			$data_sets[] = array('permission.permission_set', array('ROLE_MOD_FULL', array('m_crml_view_logs', 'm_crml_manage_user_cash')));
		}

		if($this->role_exists('ROLE_USER_STANDARD'))
		{
			$data_sets[] = array('permission.permission_set', array('ROLE_USER_STANDARD', array('u_crml_earn', 'u_crml_donate', 'u_crml_tip', 'u_crml_bonus', 'u_crml_login_increment')));
		}

		if($this->role_exists('ROLE_USER_FULL'))
		{
			$data_sets[] = 
			array('permission.permission_set', array('ROLE_USER_FULL', array('u_crml_earn', 'u_crml_donate', 'u_crml_tip', 'u_crml_bonus', 'u_crml_login_increment')));
		}

		return $data_sets;
	}

	public function revert_data()
	{
		$this->remove_log_type_data();
		return array();
	}

	public function add_log_type_data()
	{
		$sql_array = array(
			array(
				'log_type_name' => 'donate',
				'log_type_ucp_visible' => 1
			),
			array(
				'log_type_name' => 'tip',
				'log_type_ucp_visible' => 1
			),
		);

		$this->db->sql_multi_insert($this->table_prefix . 'caramel_log_types', $sql_array);
	}

	public function remove_log_type_data()
	{
		$sql = 'DELETE * FROM ' . $this->table_prefix . 'caramel_log_types';
		$this->sql_query($sql);
	}

	protected function role_exists($role)
	{
	    $sql = 'SELECT COUNT(role_id) AS role_count
	        FROM ' . ACL_ROLES_TABLE . "
	        WHERE role_name = '" . $this->db->sql_escape($role) . "'";
	    $result = $this->db->sql_query_limit($sql, 1);
	    $role_count = $this->db->sql_fetchfield('role_count');
	    $this->db->sql_freeresult($result);
	    return $role_count > 0;
	}
}
