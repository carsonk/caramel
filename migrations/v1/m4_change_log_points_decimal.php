<?php
/**
 *
 * Caramel
 *
 * @copyright (c) 2017 carsonk
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace carsonk\caramel\migrations\v1;

class m4_change_log_points_decimal extends \phpbb\db\migration\migration
{
    public function update_schema()
    {
        return array(
            'change_columns'    => array(
                $this->table_prefix . 'caramel_log'        => array(
                    'log_points'        => array('DECIMAL:10', 0),
                ),
            ),
        );
    }

    public function revert_schema()
    {
        return array(
            'change_columns'    => array(
                $this->table_prefix . 'caramel_log'        => array(
                    'log_points'        => array('UINT', 0),
                ),
            ),
        );
    }
}
